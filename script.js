const url = "https://rickandmortyapi.com/api/character";
fetch(url)
  .then((res) => res.json())
  .then((data) => {
    const rawData = data.results;

    createTen(rawData);

    window.onscroll = function () {
      addScroll(rawData);
    };

    // DELETE CARD ON CLICK
    document
      .getElementById("container")
      .addEventListener("click", function () {
        let target = event.target;
        let length = rawData.length;

        for (i = 0; i < length; i++) {          
          if (target.name == i + 1) {
            let id = target.name;
            let div = document.getElementById(`${i+1}`);
            div.remove();
            console.log(rawData.length);            
            rawData.forEach((element, index) => {              
              if (element.id == id) {                
                rawData.splice(index, 1);                
              }
            });
          }
        }; 
      });

      // SORT BY EPISODE #
      document
      .getElementById("episode-sort-id")
      .addEventListener("click", function () {        
        let length = document.getElementsByClassName("card-wrp").length;
        for (i = 0; i < length; i++) {          
          let div = document.querySelector(".card-wrp");
          div.remove();
        }
        numberOfEpisodesSort(rawData);
        createAll(rawData);
      });

      // SORT BY TIME ASC
      document
      .getElementById("date-sort-asc")
      .addEventListener("click", function () {        
        let length = document.getElementsByClassName("card-wrp").length;
        for (i = 0; i < length; i++) {          
          let div = document.querySelector(".card-wrp");
          div.remove();
        }
        dateSortAsc(rawData);
        createAll(rawData);
      });

      // SORT BY TIME DESC
      document
      .getElementById("date-sort-desc")
      .addEventListener("click", function () {        
        let length = document.getElementsByClassName("card-wrp").length;
        for (i = 0; i < length; i++) {          
          let div = document.querySelector(".card-wrp");
          div.remove();
        }
        dateSortDesc(rawData);
        createAll(rawData);
      });

  })
  .catch((error) => {
    console.log(JSON.stringify(error));
  });


function createEl(character) {
  let container = document.getElementById("container");

  let wrapper = document.createElement("div");
  wrapper.className = "card-wrp";
  wrapper.id = character.id.toString();  

  let charImg = document.createElement("img");
  charImg.src = character.image;
  charImg.className = "image";

  let charName = document.createElement("p");
  charName.innerText = character.name;
  charName.className = "title caps";

  let charSpecies = document.createElement("p");
  charSpecies.innerText = character.species;
  charSpecies.className = "title";

  let charCreatedTitle = document.createElement("p");
  charCreatedTitle.innerText = `Created`;
  charCreatedTitle.className = "title";

  let charCreated = document.createElement("p");
  charCreated.innerText = character.created.slice(11, 19);
  charCreated.className = "text";

  let charEpisodesTitle = document.createElement("p");
  charEpisodesTitle.innerText = `Episodes list`;
  charEpisodesTitle.className = "title";

  let charEpisodes = document.createElement("p");
  charEpisodes.innerText = `${episodeNumber(character.episode)}`;
  charEpisodes.className = "text";

  let charLocationTitle = document.createElement("p");
  charLocationTitle.innerText = `Location`;
  charLocationTitle.className = "title";

  let charLocation = document.createElement("p");
  charLocation.innerText = `${character.location.name}`;
  charLocation.className = "text";

  let deleteBtn = document.createElement("button");
  deleteBtn.innerText = "X";  
  deleteBtn.className = "delete-btn";
  deleteBtn.setAttribute("name", character.id.toString());

  wrapper.appendChild(deleteBtn);
  wrapper.appendChild(charImg);
  wrapper.appendChild(charName);
  wrapper.appendChild(charSpecies);
  wrapper.appendChild(charCreatedTitle);
  wrapper.appendChild(charCreated);
  wrapper.appendChild(charEpisodesTitle);
  wrapper.appendChild(charEpisodes);
  wrapper.appendChild(charLocationTitle);
  wrapper.appendChild(charLocation);

  container.appendChild(wrapper);
}

const openingElementsNumber = 10;

function createTen(Data) {
  return Data.slice(0, openingElementsNumber).map((character) => {
    createEl(character);
  });
}

function createAll(Data) {
  return Data.map((character) => {
    createEl(character);
  });
}

function createOne(Data, i) {
  return Data.slice(i, i + 1).map((character) => {
    createEl(character);
  });
}

function addScroll(Data) {
  if (document.getElementsByClassName("card-wrp").length < 18) {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {      
      let length = document.getElementsByClassName("card-wrp").length;
        for (i = 0; i < length; i++) {          
          let div = document.querySelector(".card-wrp");
          div.remove();
        }
        return Data.map((character) => {
          createEl(character);
        });  
    }
  }
}

function episodeNumber(episode) {
  let episodesList = [];
  episode.map((string) => {
    let stringParts = string.split("/");
    episodesList.push(stringParts[stringParts.length - 1]);
  });
  return episodesList.join(", ");
}

function dateSortAsc (Data){
  Data.sort(function (a, b) {
    if (a.created.slice(11, 19) > b.created.slice(11, 19)) {
      return -1;
    }
    if (a.created.slice(11, 19) < b.created.slice(11, 19)) {
      return 1;
    }
    return 0;
  });
}

function dateSortDesc (Data){
  Data.sort(function (a, b) {
    if (a.created.slice(11, 19) > b.created.slice(11, 19)) {
      return 1;
    }
    if (a.created.slice(11, 19) < b.created.slice(11, 19)) {
      return -1;
    }
    return 0;
  });
}

function numberOfEpisodesSort(Data) {
  Data.sort(function (a, b) {
    if (a.episode.length > b.episode.length) {
      return -1;
    }
    if (a.episode.length < b.episode.length) {
      return 1;
    }
    return 0;
  }); 
}

function deleteElm (Data, id) {
  for (i = 0; i < rawData.length; i++){
    if (data[i].character.id == id) {
      Data.splice(i, 1);
    }
  }     
};

